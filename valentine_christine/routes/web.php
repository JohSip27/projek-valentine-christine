<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('redirects' , [HomeController::class , 'index']);
Route::get('/buatjadwal' , [HomeController::class , 'buatjadwal']);
route::get('/lihatjadwal' , [HomeController::class , 'lihatjadwal']);
route::get('/lihatdokumen' , [HomeController::class , 'lihatdokumen']);
route::get('/uploaddokumen' , [HomeController::class , 'uploaddokumen']);
route::get('/daftarakun' , [HomeController::class , 'daftarakun']);
route::post('/insertjadwal' , [HomeController::class , 'insertjadwal']);
route::post('/insertdokumen' , [HomeController::class , 'insertdokumen']);
route::get('/installjadwal/{id}' , [HomeController::class , 'installjadwal']);
route::post('/updatejadwal/{id}' , [HomeController::class , 'updatejadwal']);
route::get('/updatedokumen/{id}' , [HomeController::class , 'updatedokumen']);
Route::get('/deletejadwal/{id}' ,[HomeController::class , 'deletejadwal']);
Route::get('/deletedokumen/{id}' ,[HomeController::class , 'deletedokumen']);
route::post('/perbaharuidokumen/{id}' , [HomeController::class , 'perbaharuidokumen']);


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
