<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class uploaddokumen extends Model
{
    use HasFactory;
    protected $table = 'uploaddokumen';
    protected $fillable = ['namadokumen' ,'kategoridokumen', 'file' , 'keterangan'];
}
