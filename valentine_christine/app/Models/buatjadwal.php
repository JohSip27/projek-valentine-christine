<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class buatjadwal extends Model
{
    use HasFactory;
    protected $table = 'jadwal';
    protected $fillable = ['namajadwal' ,'tanggaljadwal', 'file' , 'keterangan'];
}
