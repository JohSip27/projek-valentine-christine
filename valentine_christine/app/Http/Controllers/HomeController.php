<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\buatjadwal;
use App\Models\uploaddokumen;

class HomeController extends Controller
{
    public function index()
    {
        $role=Auth::user()->role;

        if($role=='1') {
            $dokumen = uploaddokumen::all();
            $data = buatjadwal::all();
            return view ('admin' , compact('data' , 'dokumen'));//halaman khusus christine
        }
        if($role=='2') {
            
            return view('pengguna');//halaman pengguna
        }
        else {

            return view('lihat');//halaman penglihat
        }
    }

    public function buatjadwal()
    {
        return view('buatjadwal');
    }

    public function lihatjadwal()
    {
        $data = buatjadwal::all();
        return view('lihatjadwal' , compact('data'));
    }
    
    public function insertjadwal(Request $request)
    {
        $data = buatjadwal::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('jadwal/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return redirect('/buatjadwal')->with('success','Success , Jadwal Berhasil Dibuat , Lihat dan jangan lupa dilakukan' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }

    public function uploaddokumen()
    {
        return view('uploaddokumen');
    }

    public function insertdokumen(Request $request)
    {
        $data = uploaddokumen::create($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('dokumen/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return redirect('/buatjadwal')->with('success','Success , Dokumen Berhasil Diupload' , 'Silahkan tunggu 1x24 jam akun akan dikirimkan via Email');
    }

    public function lihatdokumen()
    {
        $data = uploaddokumen::all();
        return view('lihatdokumen' , compact('data'));
    }

    public function installjadwal($id)
    {
        $data = buatjadwal::find($id);
        return view('editjadwal' , compact ('data'));
    }

    public function updatejadwal(Request $request , $id)
    {
        $data = buatjadwal::find($id);
        $data->update($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('artefak/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return view('sukses')->with('success','Success , Data Berhasil Di Update');
    }

    public function deletejadwal($id)
    {
        $data = buatjadwal::find($id);
        $data->delete();
        return view('hapus');
    }

    public function updatedokumen($id)
    {
        $data = uploaddokumen::find($id);
        return view('updatedokumen' , compact('data'));
    }

    public function perbaharuidokumen(Request $request , $id)
    {
        $data = uploaddokumen::find($id);
        $data->update($request->all());
        if ($request->hasFile('file')) {
            $request->file('file')->move('dokumen/', $request->file('file')->getClientOriginalName());
            $data->file = $request->file('file')->getClientOriginalName();
            $data->save();
        }
        return view('sukses')->with('success','Success , Data Berhasil Di Update');
    }

    public function deletedokumen($id)
    {
        $data = uploaddokumen::find($id);
        $data->delete();
        return view('hapus');
    }
}
